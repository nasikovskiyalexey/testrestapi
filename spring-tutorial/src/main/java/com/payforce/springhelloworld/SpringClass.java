package com.payforce.springhelloworld;

import javax.naming.Name;

public class SpringClass {
    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    private String name;
}
