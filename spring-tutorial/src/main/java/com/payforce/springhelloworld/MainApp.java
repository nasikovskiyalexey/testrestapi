package com.payforce.springhelloworld;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;


public class MainApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("beans-configuration.xml");
        SpringClass myClas = (SpringClass) context.getBean("helloWorld");
        System.out.println(myClas.getName());
    }
}
