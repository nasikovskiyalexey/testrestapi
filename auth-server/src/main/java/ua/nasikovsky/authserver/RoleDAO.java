package ua.nasikovsky.authserver;

import java.sql.SQLException;
import java.util.Collection;

public interface RoleDAO {
    public Role getRoleById(int role_id) throws SQLException;
    public Collection getAllRoles() throws SQLException;
    public Collection getRolesByUser(Role role) throws SQLException;
}
