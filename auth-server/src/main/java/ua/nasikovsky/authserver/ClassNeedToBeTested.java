package ua.nasikovsky.authserver;

public class ClassNeedToBeTested {

    public String SuperRiskMethod(int i) throws UnbelivableException{

        double a = Math.random()*13;
        double b = Math.random()*-10;
        if(Math.abs(b)>a) return "b";
        if(Math.abs(b)<a) return "a";
        throw new UnbelivableException();
    }
}
