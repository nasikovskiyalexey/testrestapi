package ua.nasikovsky.authserver;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Set;

public class User {
    @JsonProperty("id")
    private int id;
    @JsonProperty("login")
    private String login;
    @JsonProperty("password")
    private String password;
//    @JsonProperty("roles")
    private Set<Role> roles = new HashSet<Role>();

    public User() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set getRoles() {
        return roles;
    }

    public void setRoles(Set roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("User{id=" + id +
                ", login=" + login +
                ", password=" + password + ", roles={");
        for(Role role : roles) {
            sb.append(role.toString() + " ");
        }
        sb.append("}}");
        return sb.toString();
    }
}
