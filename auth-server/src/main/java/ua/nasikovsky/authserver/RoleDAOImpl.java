package ua.nasikovsky.authserver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaQuery;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class RoleDAOImpl implements RoleDAO {
    Logger logger = LogManager.getLogger(RoleDAOImpl.class);

    @Override
    public Role getRoleById(int role_id) throws SQLException {
        logger.info("Method \"getRoleById\" is invoked \"role_id\"=" + role_id);

        Session session = null;
        Role role = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            logger.debug("Session is opened");

            role = (Role) session.load(Role.class, role_id);
            logger.info("Role from database " + role.toString());

        } catch (Exception e) {
            logger.error(e);
            throw new SQLException("IN getRoleById");
//            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            logger.debug("Session is closed");
        }
        return role;
    }

    @Override
    public Collection getAllRoles() throws SQLException {
        logger.info("Method \"getAllRoles\" is invoked");

        Session session = null;
        List<Role> roles = new ArrayList<Role>();
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            logger.debug("Session is opened");

            CriteriaQuery<Role> criteriaQuery= session.getCriteriaBuilder().createQuery(Role.class);
            criteriaQuery.from(Role.class);

            roles = session.createQuery(criteriaQuery).getResultList();

            StringBuilder sb = new StringBuilder();
            for(Role role : roles) {
                sb.append(role.toString() + " ");
            }
            logger.info("Roles from database " + sb.toString());

        } catch (Exception e) {
            logger.error(e);
            throw new SQLException("IN getAllRoles");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            logger.debug("Session is closed");
        }
        return roles;
    }

    @Override
    public Collection getRolesByUser(Role role) throws SQLException {
        return null;
    }

}
