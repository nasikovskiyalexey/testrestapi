package ua.nasikovsky.authserver;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class Role {

    public static final String CLIENT_ROLE_NAME = "client";
    public static final String ADMIN_ROLE_NAME = "admin";
    public static final int CLIENT_ROLE_id = 1;
    public static final int ADMIN_ROLE_id = 2;

    @JsonProperty("id")
    private int id;
    @JsonProperty("role")
    private String role;
//    private Set users = new HashSet();

    public Role() {
    }

    public Role(int id, String role) {
        this.id = id;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }


    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", role='" + role + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role1 = (Role) o;
        return getId() == role1.getId() &&
                Objects.equals(getRole(), role1.getRole());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRole());
    }
}
