package ua.nasikovsky.authserver;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public class UserDAOImpl implements UserDAO {

    Logger logger = LogManager.getLogger(UserDAOImpl.class);

    @Override
    public void addUser(User user) throws SQLException {
        logger.info("Method \"addUser\" is invoked \"user\"=" + user.toString());

        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            logger.debug("Session is opened");

            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e);
            throw new SQLException("IN addUser");
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            logger.debug("Session is closed");
        }

    }

    @Override
    public void updateUser(int user_id, User user) throws SQLException {

    }

    @Override
    public User getUserById(int user_id) throws SQLException {
        logger.info("Method \"getUserById\" is invoked \"user_id\"=" + user_id);

        Session session = null;
        User user = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            logger.debug("Session is opened");

            user = (User) session.load(User.class, user_id);
            logger.info("User from database " + user.toString());

        } catch (Exception e) {
            logger.error(e);
            throw new SQLException("IN getUserById");
//            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            logger.debug("Session is closed");
        }
        return user;
    }

    @Override
    public List<User> getUserByLogin(String login) throws SQLException {
        logger.info("Method \"getUserByLogin\" is invoked \"login\"=" + login);

        Session session = null;
        List<User> userList = null;
        try {
            session = HibernateUtil.getSessionFactory().openSession();
            logger.debug("Session is opened");

            String hql = "FROM User " +
                    "WHERE login = :login";

            Query query = session.createQuery(hql);
            query.setParameter("login", login);
            userList = query.getResultList();

            for (User user : userList) {
                logger.info("User from database " + user.toString());
            }
        } catch (Exception e) {
            logger.error(e);
            throw new SQLException("IN getUserByLogin");
//            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка 'findById'", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
            logger.debug("Session is closed");
        }
        return userList;
    }

    @Override
    public Collection getAllUsers() throws SQLException {
        return null;
    }

    @Override
    public void deleteUser(User user) throws SQLException {
        logger.info("Method \"addUser\" is invoked \"user\"=" + user.toString());
        Session session = null;

        try {
            session = HibernateUtil.getSessionFactory().openSession();
            logger.debug("Session is opened");

            session.beginTransaction();
            session.delete(user);
            session.getTransaction().commit();
        } catch (Exception e) {
            logger.error(e);
            throw new SQLException("IN deleteUser");
        }

    }

    @Override
    public Collection getUsersByDriver(Role role) throws SQLException {
        return null;
    }
}
