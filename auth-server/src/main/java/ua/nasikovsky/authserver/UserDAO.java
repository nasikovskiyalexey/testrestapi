package ua.nasikovsky.authserver;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

public interface UserDAO {
    public void addUser(User user) throws SQLException;
    public void updateUser(int user_id, User user) throws SQLException;
    public User getUserById(int user_id) throws SQLException;
    public List<User> getUserByLogin(String login) throws SQLException;
    public Collection getAllUsers() throws SQLException;
    public void deleteUser(User user) throws SQLException;
    public Collection getUsersByDriver(Role role) throws SQLException;
}
