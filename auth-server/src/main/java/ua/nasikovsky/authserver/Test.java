package ua.nasikovsky.authserver;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.Key;

public class Test {
    public static void main(String[] args){
        String jws = Jwts.builder()
                .setSubject("user/1")
                .claim("name", "Some Name")
                .claim("scope", "admin")
                .signWith(SignatureAlgorithm.HS256, "secretpass".getBytes(StandardCharsets.UTF_8)).compact();

        System.out.println(jws);

        Jws<Claims> claims = Jwts.parser().setSigningKey("secretpass".getBytes(StandardCharsets.UTF_8)).parseClaimsJws(jws);
        System.out.println(claims.getBody().getSubject());


    }
}
