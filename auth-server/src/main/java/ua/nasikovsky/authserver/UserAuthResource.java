package ua.nasikovsky.authserver;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Path("/user")

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserAuthResource {

    private Logger logger = LogManager.getLogger(UserAuthResource.class);

    private static String SECRET  = "somesecret";
    private UserDAO userDAOImpl;

    public void setUserDAOImpl(UserDAO userDAOImpl) {
        this.userDAOImpl = userDAOImpl;
    }

    @POST
    @Path("/login")
    public JWTDOM login(User user) {

        logger.info("Method POST by path: /login is invoked");
        logger.info("Parameter " + user.toString());
        String jwts = null;
        try {

            User db_user = userDAOImpl.getUserById(user.getId());
            logger.debug("End of db connection");

            logger.debug("User login is equal = " + db_user.getLogin().equals(user.getLogin()));
            if(!db_user.getLogin().equals(user.getLogin())) return null;

            logger.debug("User password is equal = " + db_user.getPassword().equals(EncriptionUtil.md5Apache(user.getPassword())));

            if(!db_user.getPassword().equals(EncriptionUtil.md5Apache(user.getPassword()))) return null;
            Set<Role> roles = user.getRoles();

            logger.info("User have been validated");

            roles = user.getRoles();


            StringBuilder stringBuilder = new StringBuilder();
            for(Role role : roles) {
                stringBuilder.append(role.getRole()+ "/");
            }

            jwts = Jwts.builder()
                    .setSubject(user.getLogin())
                    .claim("scope", stringBuilder.toString())
                    .signWith(SignatureAlgorithm.HS256, SECRET.getBytes(StandardCharsets.UTF_8)).compact();

            logger.info("Token is created: " + jwts);

        } catch (SQLException e) {
            logger.error(e);
            e.printStackTrace();
        }

        JWTDOM jwtDom = new JWTDOM();
        jwtDom.setToken(jwts);
        return jwtDom;
    }

    @POST
    @Path("/signin")
    public User signIn(User user) {
        logger.info("Method POST by path: /signin is invoked");
        logger.info("Parameter " + user.toString());
        try {

            if(!userDAOImpl.getUserByLogin(user.getLogin()).isEmpty()) {
//                TODO: some error
            }
            String password = user.getPassword();
            user.setPassword(EncriptionUtil.md5Apache(user.getPassword()));

            Set<Role> roles = new HashSet();
            roles.add(new Role(Role.CLIENT_ROLE_id, Role.CLIENT_ROLE_NAME));

            userDAOImpl.addUser(user);

            List<User> users = userDAOImpl.getUserByLogin(user.getLogin());

            user = users.get(0);
            user.setPassword(password);

        } catch (SQLException e) {
            logger.error(e);
            e.printStackTrace();
        }
        return user;
    }

    @POST
    @Path("/testlogin")
    public JWTDOM testLogin(User user) {

        logger.info("Method POST by path: /testlogin is invoked");
        logger.info("Parameter " + user.toString());
        String jwts = null;
        try {

            User db_user = userDAOImpl.getUserById(user.getId());
            logger.debug("End of db connection");

            if(!db_user.getLogin().equals(user.getLogin())) return null;

            if(!db_user.getPassword().equals(user.getPassword())) return null;
            Set<Role> roles = user.getRoles();

            logger.info("User have been validated");

            roles = user.getRoles();


            StringBuilder stringBuilder = new StringBuilder();
            for(Role role : roles) {
                stringBuilder.append(role.getRole()+ "/");
            }

            jwts = Jwts.builder()
                    .setSubject(user.getLogin())
                    .claim("scope", stringBuilder.toString())
                    .signWith(SignatureAlgorithm.HS256, SECRET.getBytes(StandardCharsets.UTF_8)).compact();

            logger.info("Token is created: " + jwts);

        } catch (SQLException e) {
            logger.error(e);
            e.printStackTrace();
        }

        JWTDOM jwtDom = new JWTDOM();
        jwtDom.setToken(jwts);
        return jwtDom;
    }
}
