package ua.nasikovsky.authserver;

import org.apache.commons.codec.digest.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class EncriptionUtil {
    public static String base64Decode(String ecodedStr) {
        byte[] decodedbytes = ecodedStr.getBytes(StandardCharsets.UTF_8);
        decodedbytes = Base64.getDecoder().decode(decodedbytes);
        return new String(decodedbytes, StandardCharsets.UTF_8);
    }

    public static String md5Apache(String str) {
        return DigestUtils.md2Hex(str);
    }

    public String base64Encode(String decodedStr) {
        return null;
    }
}
