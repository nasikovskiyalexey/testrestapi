package ua.nasikovsky.authserver;

import com.fasterxml.jackson.annotation.JsonProperty;

public class JWTDOM {
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @JsonProperty
    private String token;


}
