package ua.nasikovsky.authserver;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class AuthServerTest {

    public RoleDAO roleDAO;
    public UserDAO userDAO;
    List<Role> checkRoles;
    Session session;
    Logger logger;

    @Before
    public void initiationMeth(){
        roleDAO = new RoleDAOImpl();
        userDAO = new UserDAOImpl();
        checkRoles = new ArrayList<Role>();
        checkRoles.add(new Role(Role.CLIENT_ROLE_id, Role.CLIENT_ROLE_NAME));
        checkRoles.add(new Role(Role.ADMIN_ROLE_id, Role.ADMIN_ROLE_NAME));
        session = HibernateUtil.getSessionFactory().openSession();
        logger = LogManager.getLogger(AuthServerTest.class);
    }

    @Test
    public void sessionTest() {
        logger.info("Test \"sessionTest\" started");

        assertNotNull("Hibernate session is null", session);
        try {
            List<Role> roles = (List<Role>) roleDAO.getAllRoles();
            assertNotNull("List<Role> roles is null", roles);
            assertTrue("roles are not equal",isEqual(roles));

        } catch (SQLException e) {
            fail(e.getMessage());

        }
        logger.info("Test \"sessionTest\" succeed");
    }

    @Test
    public void getRegisteredUserByLoginTest() {
        logger.info("Test \"getRegisteredUserByLoginTest\" started");

        try {
            List<User> userList = userDAO.getUserByLogin("login65464");
            assertFalse("User is null ", userList.isEmpty());

        } catch (SQLException e) {
            fail(e.getMessage());
        }
        logger.info("Test \"getRegisteredUserByLoginTest\" succeed");
    }

    @Test
    public void getUnregisteredUserByLoginTest() {
        logger.info("Test \"getUnregisteredUserByLoginTest\" started");

        try {
            List<User> userList = userDAO.getUserByLogin("testUnregistered");
            assertTrue("User is null ", userList.isEmpty());

        } catch (SQLException e) {
            fail(e.getMessage());
        }
        logger.info("Test \"getUnregisteredUserByLoginTest\" succeed");
    }

    @Test
    public void addDeleteUserTest() {
        logger.info("Test \"addDeleteUserTest\" started");

        User user = new User();
        user.setPassword(EncriptionUtil.md5Apache("test"));
        user.setLogin("test");

        try {
            userDAO.addUser(user);
            List<User> users = userDAO.getUserByLogin(user.getLogin());
            for (User usertmp: users) {
                logger.debug("Content of users list: " + usertmp.toString());
            }

            assertFalse("\"users\" from DB is null", users.isEmpty());

            User user1 = users.get(0);
            assertTrue("Added user is not equal to came from DB",user1.getLogin().equals(user.getLogin())
                    && user1.getPassword().equals(user.getPassword()));

            userDAO.deleteUser(user);
        } catch (SQLException e) {
            fail(e.getMessage());
        }
        logger.info("Test \"addDeleteUserTest\" succeed");
    }

    private boolean isEqual(List<Role> roles) {
        if(roles.size() != checkRoles.size()) return false;
        for(int i = 0; i<roles.size(); i++){
            boolean found = false;
            for(int j = 0; j<roles.size()&&!found; j++){
                if(roles.get(i).getId()==checkRoles.get(j).getId()) found = true;
            }
            if(!found) return false;
        }
        return true;
    }
}
