package test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger("EventLogger");
        logger.info("my message");
        logger.debug("my message" + " with very intrasting data");
        logger.trace(args);

        Logger consLogger = LogManager.getLogger("ConsoleEventLogger");
        consLogger.info("my console message");
    }
}
