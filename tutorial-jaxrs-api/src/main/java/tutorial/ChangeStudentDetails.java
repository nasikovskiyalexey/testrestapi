package tutorial;

public interface ChangeStudentDetails {
    Student changeName(Student student);
    Student getName();
}
