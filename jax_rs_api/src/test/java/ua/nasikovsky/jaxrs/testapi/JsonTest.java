package ua.nasikovsky.jaxrs.testapi;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.junit.Before;
import org.junit.Test;
import ua.nasikovsky.jaxrs.testapi.security.AuthorizationException;
import ua.nasikovsky.jaxrs.testapi.security.ExceptionForm;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class JsonTest {

    private ObjectMapper errorObjectMapper;
    private ObjectMapper objectMapper;
    private String jsonAuthException;
    @Before
    public void init() {
        errorObjectMapper = new ObjectMapper();
        errorObjectMapper.disable(MapperFeature.AUTO_DETECT_CREATORS,
                MapperFeature.AUTO_DETECT_FIELDS,
                MapperFeature.AUTO_DETECT_GETTERS,
                MapperFeature.AUTO_DETECT_IS_GETTERS);
        // to prevent an exception when classes have no annotated properties
        errorObjectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
        objectMapper = new ObjectMapper();
        jsonAuthException = "{\"errorMessageKey\":" + ExceptionForm.UNATHORIZED_EXCEPTION +
                ",\"messageDescription\":\"" + AuthorizationException.AUTHORIZATION_EXCEPTION_MESSAGE + "\"}";
    }

    @Test
    public void testAuthorizationExceptionSerialization() {
        AuthorizationException authorizationException = new AuthorizationException(AuthorizationException.AUTHORIZATION_EXCEPTION_MESSAGE);
        try {
            String jsonAuthException = errorObjectMapper.writeValueAsString(authorizationException);

            System.out.println(jsonAuthException);
            System.out.println(this.jsonAuthException);
            assertTrue("Authorization exception json string not valid" + jsonAuthException + " and " + this.jsonAuthException,
                    this.jsonAuthException.equals(jsonAuthException));


        } catch (JsonProcessingException e) {
            fail(e.getMessage());
        }

    }
}
