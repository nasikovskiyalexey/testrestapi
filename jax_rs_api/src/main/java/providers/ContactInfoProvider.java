package providers;

import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import ua.nasikovsky.jaxrs.testapi.neverknowwhatinside.ContactInfo;

@Provider
@Produces(MediaType.TEXT_HTML)
public class ContactInfoProvider implements MessageBodyWriter<ContactInfo>{


    @Override
    public boolean isWriteable(Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return true;
    }

    @Override
    public long getSize(ContactInfo contactInfo, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType) {
        return 1;
    }

    @Override
    public void writeTo(ContactInfo contactInfo, Class<?> aClass, Type type, Annotation[] annotations, MediaType mediaType, MultivaluedMap<String, Object> multivaluedMap, OutputStream outputStream) throws IOException, WebApplicationException {
        String res = "{\"error\": \"contactInfo NullPointerException\" }";
        if(contactInfo != null) {
            res = "{ \"id\": \"" + contactInfo.getId() + "\", " + "\"name\": \"" + contactInfo.getName() + "\", " +"\"sex\": \"" + contactInfo.getSex() + "\" }";
        }
        PrintWriter pw = new PrintWriter(outputStream);
        pw.println(res);
    }
}
