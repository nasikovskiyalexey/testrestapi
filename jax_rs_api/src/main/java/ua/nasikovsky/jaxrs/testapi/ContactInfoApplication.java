package ua.nasikovsky.jaxrs.testapi;

//import org.apache.cxf.jaxrs.provider.json.DataBindingJSONProvider;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class ContactInfoApplication extends Application {

    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(ContactsResource.class);
        //classes.add(DataBindingJSONProvider.class);
        return classes;
    }
}
