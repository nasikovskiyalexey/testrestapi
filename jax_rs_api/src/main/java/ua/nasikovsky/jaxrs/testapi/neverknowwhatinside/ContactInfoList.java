package ua.nasikovsky.jaxrs.testapi.neverknowwhatinside;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ContactInfoList {

    @JsonProperty("contactInfoList")
    List<ContactInfo> contactInfoList;

    public List<ContactInfo> getContactInfoList() {
        return contactInfoList;
    }

    public void setContactInfoList(List<ContactInfo> contactInfoList) {
        this.contactInfoList = contactInfoList;
    }
}
