package ua.nasikovsky.jaxrs.testapi.security;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;
//
//@JsonAutoDetect(
//        fieldVisibility = JsonAutoDetect.Visibility.NONE,
//        setterVisibility = JsonAutoDetect.Visibility.NONE,
//        getterVisibility = JsonAutoDetect.Visibility.NONE,
//        isGetterVisibility = JsonAutoDetect.Visibility.NONE,
//        creatorVisibility = JsonAutoDetect.Visibility.NONE
//)
public class AuthorizationException extends ExceptionForm {



    public static final String AUTHORIZATION_EXCEPTION_MESSAGE = "Not valid login or password";
    public static final String TOKEN_AUTHORIZATON_EXCEPTIO_MESSAGE = "Token is not valid";

    public AuthorizationException(String messageDescription) {
        super(messageDescription, ExceptionForm.UNATHORIZED_EXCEPTION);
    }

}
