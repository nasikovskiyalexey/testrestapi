package ua.nasikovsky.jaxrs.testapi.filters;


import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Enumeration;

public class AuthFilter implements Filter {

    private Logger logger = LogManager.getLogger(AuthFilter.class);
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        //System.out.println("MY_INFO:||||||||||| " + "filter init");
        if (logger.getLevel().compareTo(Level.DEBUG) > 0) {
            logger.info("Info msg");
            logger.debug("Debug msg");
        }


    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        System.out.println("MY_INFO:||||||||||| " + "do");

        HttpServletRequest request = (HttpServletRequest)servletRequest;
        System.out.println("MY_INFO:||||||||||| " + "get header:");
        String authToken = request.getHeader(HttpHeaders.AUTHORIZATION);
        System.out.println("MY_INFO:||||||||||| " + authToken);
//        if(authToken!=null&&authToken!="") {
//
//        } else {
            request.getRequestDispatcher("/service/logining/testLogin").forward(servletRequest,servletResponse);
//        }

        System.out.println("MY_INFO:||||||||||| " + "after forward");
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        System.out.println("MY_INFO:||||||||||| " + "destroy");
    }
}
