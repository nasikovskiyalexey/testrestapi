package ua.nasikovsky.jaxrs.testapi.security;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class RestExceptionHandler {

    @ResponseBody
    @ExceptionHandler(AuthorizationException.class)
    public String handleAuthentificationException(AuthorizationException ex, HttpServletResponse response) throws IOException {
        response.setStatus(ExceptionBox.UNATHORIZED_EXCEPTION);
        return mapExcetionBox(ExceptionBox.UNATHORIZED_EXCEPTION, ex.getMessage());
    }

    private String mapExcetionBox(int erroKey, String message) throws IOException {

        ExceptionBox exceptionBox = new ExceptionBox(erroKey, message);

        String json = new ObjectMapper().writeValueAsString(exceptionBox);

        System.out.println("ExceptionJson: " + json);
        return json;

    }
}
