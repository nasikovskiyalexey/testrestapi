package ua.nasikovsky.jaxrs.testapi.filters;


import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.*;
import org.apache.cxf.jaxrs.impl.ResponseImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import ua.nasikovsky.jaxrs.testapi.security.AuthorizationException;

@Provider
@PreMatching
public class AuthorizationFilter implements ContainerRequestFilter {

    private static String SECRET = "somesecret";
    private Logger logger = LogManager.getLogger(AuthorizationFilter.class);

    @Autowired
    @Qualifier("errorObjectMapper")
    private ObjectMapper errorObjectMapper;

    @Override
    public void filter(ContainerRequestContext containerRequestContext) throws IOException {

        logger.debug(containerRequestContext.getClass());
        logger.debug("In container");

        String authToken = containerRequestContext.getHeaderString(HttpHeaders.AUTHORIZATION);

        if(authToken.equals(null) || authToken.equals("")) {
            logger.error("value of \"authToken\" is null or empty"+AuthorizationException.AUTHORIZATION_EXCEPTION_MESSAGE);

            String response = errorObjectMapper.writeValueAsString(new AuthorizationException(AuthorizationException.AUTHORIZATION_EXCEPTION_MESSAGE));

            containerRequestContext.abortWith(ResponseImpl.status(ResponseImpl.Status.UNAUTHORIZED).entity(response)
                    .type(MediaType.APPLICATION_JSON_TYPE).build());
        }

        try{
            authorize(authToken);
        }catch (AuthorizationException ex) {
            logger.error(AuthorizationException.AUTHORIZATION_EXCEPTION_MESSAGE);
            logger.debug(authToken);
            String response = errorObjectMapper.writeValueAsString(ex);
            logger.debug(response);
            containerRequestContext.abortWith(ResponseImpl.status(Response.Status.UNAUTHORIZED).entity(response)
                    .type(MediaType.APPLICATION_JSON_TYPE).build());
            logger.debug("End of catch");
            return;
        } finally {
            logger.debug("End of container");
        }

    }

    private void authorize(String authToken) throws AuthorizationException {

        try {
            Jws<Claims> claims = Jwts.parser().setSigningKey(SECRET.getBytes(StandardCharsets.UTF_8)).parseClaimsJws(authToken);

            logger.info("Header: "+claims.getHeader());
            logger.info("Body: "+claims.getBody());
            logger.info("Signature: "+claims.getSignature());

        } catch (Exception e) {
            logger.error(AuthorizationException.TOKEN_AUTHORIZATON_EXCEPTIO_MESSAGE);
            throw new AuthorizationException(AuthorizationException.TOKEN_AUTHORIZATON_EXCEPTIO_MESSAGE);
        }
    }
}
