package ua.nasikovsky.jaxrs.testapi.neverknowwhatinside;

//import com.fasterxml.jackson.annotation.JsonProperty;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

//import javax.xml.bind.annotation.XmlAccessType;
//import javax.xml.bind.annotation.XmlAccessorType;
//import javax.xml.bind.annotation.XmlAttribute;
//import javax.xml.bind.annotation.XmlRootElement;

//@XmlAccessorType(XmlAccessType.NONE)
//@XmlRootElement(name = "user")
public class ContactInfo {

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @JsonProperty("id")
//    @XmlAttribute(name = "id")
    private int id;
    @JsonProperty("name")
//    @XmlAttribute(name = "name")
    private String name;
    @JsonProperty("sex")
//    @XmlAttribute(name = "sex")
    private String sex;

    @JsonCreator
    public ContactInfo(
            @JsonProperty("id") int id,
            @JsonProperty("name") String name,
            @JsonProperty("sex") String sex){
        this.id = id;
        this.name = name;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "ContactInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
