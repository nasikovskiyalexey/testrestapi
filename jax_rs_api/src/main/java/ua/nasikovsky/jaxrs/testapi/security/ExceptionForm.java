package ua.nasikovsky.jaxrs.testapi.security;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.IOException;

public abstract class ExceptionForm extends IOException {

    /* Passing WrongCredentialsException with 400 HttpStatusCode */
    public static final int WRONG_CREDENTIALS_EXCEPTION = 400;

    /* Passing WrongCredentialsException with 400 HttpStatusCode */
    public static final int UNATHORIZED_EXCEPTION = 401;

    /* All exceptions related to surety records handling, SC_PAYMENT_REQUIRED 402*/
    public static final int SURETYRECORD_EXCEPTION = 402;

    /* All exceptions related to surety record templates handling*/
    public static final int SURETYRECORD_TEMPLATE_EXCEPTION = 404;

    /* Passing ValidationExseption */
    public static final int VALIDATION_EXCEPTION = 412;

    /* When OTP attempts violated allowed */
    public static final int OTP_ATTEMPTS_VIOLATION = 413;

    private int errorMessageKey;
    private String messageDescription;


    public ExceptionForm(String messageDescription, int errorMessageKey) {
        super(messageDescription);
        this.errorMessageKey = errorMessageKey;
        this.messageDescription = messageDescription;
    }

    @JsonProperty
    public int getErrorMessageKey() {
        return errorMessageKey;
    }

    @JsonProperty
    public String getMessageDescription() {
        return messageDescription;
    }
}
