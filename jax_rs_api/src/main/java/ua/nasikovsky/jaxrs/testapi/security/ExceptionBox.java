package ua.nasikovsky.jaxrs.testapi.security;

public class ExceptionBox {

    public int errorKey;
    public String errorDescription;

    /* Passing WrongCredentialsException with 400 HttpStatusCode */
    public static final int WRONG_CREDENTIALS_EXCEPTION = 400;

    /* Passing WrongCredentialsException with 400 HttpStatusCode */
    public static final int UNATHORIZED_EXCEPTION = 401;

    /* All exceptions related to surety records handling, SC_PAYMENT_REQUIRED 402*/
    public static final int SURETYRECORD_EXCEPTION = 402;

    /* All exceptions related to surety record templates handling*/
    public static final int SURETYRECORD_TEMPLATE_EXCEPTION = 404;

    /* Passing ValidationExseption */
    public static final int VALIDATION_EXCEPTION = 412;

    /* When OTP attempts violated allowed */
    public static final int OTP_ATTEMPTS_VIOLATION = 413;

    public ExceptionBox(int errorKey, String errorDescription) {
        this.errorKey = errorKey;
        this.errorDescription = errorDescription;
    }
}
