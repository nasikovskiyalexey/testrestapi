package ua.nasikovsky.jaxrs.testapi;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import ua.nasikovsky.jaxrs.testapi.neverknowwhatinside.ContactInfo;
import ua.nasikovsky.jaxrs.testapi.neverknowwhatinside.ContactInfoList;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;



@Path("/info")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ContactsResource {

    private Logger logger = LogManager.getLogger(ContactsResource.class);

    @GET
    @Path("/contacts")
    public ContactInfoList getContacts() {
        logger.info("Method GET by path: /contacts is invoked");
        List<ContactInfo> contacts = new ArrayList<>();
        contacts.add(new ContactInfo(1,"Vadim", "m"));
        contacts.add(new ContactInfo(2,"Yulia", "f"));
        contacts.add(new ContactInfo(3,"Kirill", "m"));
        ContactInfoList contactInfoList = new ContactInfoList();
        contactInfoList.setContactInfoList(contacts);
        logger.info(Arrays.toString(contacts.toArray()));
        return contactInfoList;
    }

    @GET
    @Path("/contact/{id}")
    public ContactInfo getContactById(@PathParam("id") int id) {
        logger.info("Method POST by path: /contact/{id} is invoked ||| \"id\"=" + id);
        return new ContactInfo( id, "BroById", "somebody");
    }

    @POST
    @Path("/contact")
    @Consumes(MediaType.APPLICATION_JSON)
    public ContactInfo getContats(ContactInfo contactInfo) {
        logger.info("Method POST by path: /contact is invoked");
        logger.info("Parameter " + contactInfo.toString());
        return new ContactInfo(contactInfo.getId(), contactInfo.getName(), contactInfo.getSex());
         //return new ContactInfo( 1, "BroById", "somebody");
    }
}
