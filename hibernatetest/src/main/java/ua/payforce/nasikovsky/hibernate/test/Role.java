package ua.payforce.nasikovsky.hibernate.test;

import java.util.HashSet;
import java.util.Set;

public class Role {
    private int id;
    private String role;
//    private Set users = new HashSet();

    public Role() {
    }

//    public Set getUsers() {
//        return users;
//    }
//
//    public void setUsers(Set users) {
//        this.users = users;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
