package ua.payforce.nasikovsky.hibernate.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.swing.*;
import java.util.HashSet;
import java.util.Set;

public class MyApp {
    private static SessionFactory sessionFactory;

    public static void main(String[] args) {

        try {
            sessionFactory = new Configuration().configure().buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
        Role role = new Role();
        role.setId(1);
        role.setRole("client");
        Set roles = new HashSet();
        roles.add(role);

        User user = new User();
//        user.setId(2);
        user.setLogin("login65464");
        user.setPassword("parhgfdhgfd");
        user.setRoles(roles);
//        Set users = new HashSet();
//        users.add(user);
//        role.setUsers(users);
        Session session = null;
        System.out.println("MY_INFO: its working");
            try {
            session = sessionFactory.openSession();
            session.beginTransaction();
                System.out.println("MY_INFO: but now we are trying to save user");
            session.save(user);
                System.out.println("MY_INFO: we want to get here");
            session.getTransaction().commit();
                System.out.println("MY_INFO: transaction commited");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e.getMessage(), "Ошибка при вставке", JOptionPane.OK_OPTION);
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }
}
