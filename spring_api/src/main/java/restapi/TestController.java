package restapi;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class TestController {

    @GetMapping("/data2")
    @ResponseBody
    public String testGetMethod() {
//        return "<html><body>" + "Welcome to servlet" + "</body></html>";
        return "response";
    }

    @PostMapping("/data2")
    @ResponseBody
    public ResponseClass testPostMethod() {
        ResponseClass resp = new ResponseClass();
        resp.name = "Name";
        return resp;
    }
}
